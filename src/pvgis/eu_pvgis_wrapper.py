import sys, copy
from collections import OrderedDict
from decimal import Decimal
from re import compile
from bs4 import BeautifulSoup
import requests

# CONFIGURATION

# web tool uri
service_url = "http://re.jrc.ec.europa.eu/pvgis/apps4/pvest.php"

# PV energy calculator URL
processing_url = "http://re.jrc.ec.europa.eu/pvgis/apps4/PVcalc.php"

# form parameters passed to the wrapped service
# note that the web page has different names for some parameters:
# - azimuth > aspectangle
# - slope > angle

# these are the fields the service form itself sends to the server for processing
min_params = (
  "MAX_FILE_SIZE", "pv_database", "pvtechchoice", "peakpower", "efficiency", "mountingplace", "angle", "aspectangle",
  "horizonfile", "outputchoicebuttons", "sbutton", "outputformatchoice", "optimalchoice", "latitude", "longitude", "regionname", "language"
)

# some sane defaults for those fields
FRM_DEFAULTS = {
 "MAX_FILE_SIZE": 10000,
 "pv_database": "PVGIS-classic",
 "pvtechchoice": "crystSi",
 "peakpower": 1,
 "efficiency": 14,
 "mountingplace": "free",
 "angle": 35,
 "aspectangle": 0,
 "horizonfile": "",
 "outputchoicebuttons": "window",
 "sbutton": "Calculate",
 "outputformatchoice": "window",
 "optimalchoice": "",
 "latitude": 61.0648903,
 "longitude": 28.0948104,
 "regionname": "europe",
 "language": "en_en"
}


# PARSING

# Parse the estimated losses and production figures from the HTML page.
# Uses both regexps and bs4 for now; consider this the first functional prototype.

loss_parsers= {
   # temperature and low irradiance
   "temp_and_irradiance": compile("(?<=due to temperature and low irradiance: )([0-9]+\.?[0-9]*)(?=%  \(using local ambient)"),

   # angular reflectance
   "reflectance": compile("(?<=Estimated loss due to angular reflectance effects: )([0-9]+\.?[0-9]*)(?=%)"),

   # cables, inverter etc
   "other": compile("(?<=Other losses \(cables, inverter etc.\): )([0-9]+\.?[0-9]*)(?=%)"),

   # combined PV system losses
   "combined": compile("(?<=Combined PV system losses: )([0-9]+\.?[0-9]*)(?=%)")
}


def parse_losses(pagestr):
    "parse losses into the predefined data structure using regexps"
    data = dict.fromkeys(loss_parsers)
    for dkey in loss_parsers:
       found = loss_parsers[dkey].search(pagestr)
       data[dkey] = Decimal(found.group()) if found else None
    return data


def parse_production_and_irradiation(pagestr):
   "parse monthly Ed, Em, Hd & Hm & yearly avg"

   # first, get full table including monthly, yearly avg & total for year
   soup = BeautifulSoup(pagestr, "lxml")

   # note that the table is parsed differently by different parser backends
   dtable = soup.select("table.data_table tr")[2:]

   # Ed: Average daily electricity production from the given system (kWh)
   # Em: Average monthly electricity production from the given system (kWh)
   # Hd: Average daily sum of global irradiation per square meter received by the modules of the given system (kWh/m2)
   # Hm: Average sum of global irradiation per square meter received by the modules of the given system (kWh/m2)

   irr = {"monthly":OrderedDict(), "avg":{}, "tot":{}}
   nrg = {"monthly":OrderedDict(), "avg":{}, "tot":{}}

   # strip off three last rows (empty, avg & totals) 
   for r in dtable[:-3]:
      d = r.select("td")
      m = d[0].text.strip().lower()
      em = nrg["monthly"][m] = {}
      im = irr["monthly"][m] = {}
      em["d"], em["m"], im["d"], im["m"] = [Decimal(i.text.strip()) for i in d[1:]]

   # yearly month average is the second 'last' row; there's one empty before ('third')
   # bs4 tag lists do not support slicing so need to convert to tuple; that works!
   ea = nrg["avg"]
   ia = irr["avg"]
   ea["d"], ea["m"], ia["d"], ia["m"] = [Decimal(i.text.strip()) for i in tuple(dtable[-2])[1:]]

   # tot E & H
   trow = dtable[-1].select("td")[1:]
   nrg["tot"] = Decimal(trow[0].text.strip())
   irr["tot"] = Decimal(trow[1].text.strip())

   return (nrg, irr)


# implement the plugin API
def process(power, lat, lng, direction, angle, losses=None):
   "return data provided by this plugin, as dict"

   form = copy.deepcopy(FRM_DEFAULTS)
   form["peakpower"] = power
   form["latitude"] = lat
   form["longitude"] = lng
   form["aspectangle"] = int(direction) - 180
   form["angle"] = angle
   if losses:
      form["efficiency"] = losses
    
   # ok run the wrapper & handle the result
   result = requests.post(processing_url, data=form)
   if result.status_code == requests.codes.ok:
      production_losses = parse_losses(result.text)
      (production, irradiation) = parse_production_and_irradiation(result.text)
   else:
      sys.exit("failure: %s" % result.status_code)

   return {"losses":production_losses, "production": production, "irradiation": irradiation}
