import json
from decimal import Decimal
import requests

google_geocode_url="http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false"

def address_to_coordinates(addr):
   "convert address to coordinates"

   result = requests.get(google_geocode_url % addr)

   if result.status_code != requests.codes.ok:
      raise Exception("geocoding API HTTP request problem")

   data = json.loads(result.text, parse_float=Decimal)

   if data["status"] != "OK":
      raise Exception("geocoding API failure")

   for r in data["results"]:
      if "street_address" in r["types"] or "locality" in r["types"] or "sublocality" in r["types"]:
         place = r["formatted_address"]
         lat = r["geometry"]["location"]["lat"]
         lng = r["geometry"]["location"]["lng"]
         return (place, (lat, lng))

   raise Exception("could not find a street address for %s" % addr)
   
