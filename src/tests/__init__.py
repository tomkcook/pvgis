import os, unittest, importlib
import requests


class ParametrizedTestCase(unittest.TestCase):

    def __init__(self, methodName, module=None):
        super(ParametrizedTestCase, self).__init__(methodName)
        self.module = importlib.import_module(module)

    def setUp(self):
        # import case data module and read html and json files
        module = importlib.import_module(module)
        curdir = os.path.dirname(module.__file__)
        with open(curdir + os.sep + module.__name__.split('.')[-1] + ".html") as htmlfile:
           self.estimation_result_html = htmlfile.read()
        # mock the responses
        _resp_pvgis_post = build_response(self.estimation_result_html)
        requests.post = mock.Mock(return_value=_resp_pvgis_post)

    def get_mock_response(self, content, status=requests.codes.ok, encoding="utf-8"):
       "build a fake response for use as mock return value"
       r = requests.Response()
       r.status_code=status
       r._content = content
       r.encoding = encoding
       return mock.Mock(return_value=r)

    @staticmethod
    def parametrize(testcase_klass, module):
        """ Create a suite containing all tests taken from the given
            subclass, passing them the parameter 'case'.
        """
        testloader = unittest.TestLoader()
        testnames = testloader.getTestCaseNames(testcase_klass)
        suite = unittest.TestSuite()
        for name in testnames:
            print name, module
            suite.addTest(testcase_klass(name, module))
        return suite

