from decimal import Decimal

test_addr = "Laserkatu 10, 53850 Lappeenranta"
test_lat = 61.0648903
test_lng = 28.0948104
test_losses = 14
test_direction = 180 # south
test_angle = 35
test_power = 5

# expected results
losses = {'reflectance': Decimal('3.1'), 'temp_and_irradiance': Decimal('7.3'), 'other': Decimal('14.0'), 'combined': Decimal('22.8')}
production = {'monthly': {u'mar': {'m': Decimal('76.5'), 'd': Decimal('2.47')}, u'feb': {'m': Decimal('43.2'), 'd': Decimal('1.54')}, u'aug': {'m': Decimal('100'), 'd': Decimal('3.24')}, u'sep': {'m': Decimal('67.0'), 'd': Decimal('2.23')}, u'apr': {'m': Decimal('109'), 'd': Decimal('3.64')}, u'jun': {'m': Decimal('127'), 'd': Decimal('4.22')}, u'jul': {'m': Decimal('130'), 'd': Decimal('4.19')}, u'jan': {'m': Decimal('14.0'), 'd': Decimal('0.45')}, u'may': {'m': Decimal('133'), 'd': Decimal('4.29')}, u'nov': {'m': Decimal('14.2'), 'd': Decimal('0.47')}, u'dec': {'m': Decimal('7.75'), 'd': Decimal('0.25')}, u'oct': {'m': Decimal('38.9'), 'd': Decimal('1.26')}}, 'avg': {'m': Decimal('71.7'), 'd': Decimal('2.36')}, 'tot': Decimal('860')}
irradiation = {'monthly': {u'mar': {'m': Decimal('92.2'), 'd': Decimal('2.97')}, u'feb': {'m': Decimal('50.3'), 'd': Decimal('1.80')}, u'aug': {'m': Decimal('135'), 'd': Decimal('4.37')}, u'sep': {'m': Decimal('86.5'), 'd': Decimal('2.88')}, u'apr': {'m': Decimal('138'), 'd': Decimal('4.60')}, u'jun': {'m': Decimal('172'), 'd': Decimal('5.73')}, u'jul': {'m': Decimal('178'), 'd': Decimal('5.74')}, u'jan': {'m': Decimal('16.2'), 'd': Decimal('0.52')}, u'may': {'m': Decimal('175'), 'd': Decimal('5.65')}, u'nov': {'m': Decimal('16.9'), 'd': Decimal('0.56')}, u'dec': {'m': Decimal('9.06'), 'd': Decimal('0.29')}, u'oct': {'m': Decimal('47.9'), 'd': Decimal('1.55')}}, 'avg': {'m': Decimal('93.1'), 'd': Decimal('3.06')}, 'tot': Decimal('1120')}

