from setuptools import setup, find_packages

version = '0.8'

requirements = ['requests', 'docopt', 'beautifulsoup4', 'lxml']
setup(name='pvgis',
      version=version,
      description="tool for solar power calculations",
      long_description="""\n""",
      classifiers=[], # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
      keywords='solar,pvgis,gis',
      author='Petri Savolainen',
      author_email='petri.savolainen@koodaamo.fi',
      url='',
      license='GPL3',
      packages=find_packages(exclude=['ez_setup', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=requirements,
      entry_points= {
        'console_scripts': [
            'pvgis = pvgis.cmdline:calculate_pv_energy',
        ],
      },
      extras_require={"test":requirements + ["mock"]}
)
