This library provides programmatic access to the pvgis tool at:

http://re.jrc.ec.europa.eu/pvgis/apps4/pvest.php

While the data is freely usable, as per http://re.jrc.ec.europa.eu/pvgis/download/download.htm,
users of data are expected to cite the source:

PVGIS © European Communities, 2001-2012

Please note that this library does not perform any caching (yet). Use the library responsibly!

If you generate subsets of pvgis data using this library, PLEASE share them so others don't have to
repeat the effort and cause extra load to the service.

Even better, rather than using this library, use the original data and roll your own services and
databases straight from source:

http://re.jrc.ec.europa.eu/pvgis/download/download.htm

Thanks!
